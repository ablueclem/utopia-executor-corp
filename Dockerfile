FROM python:3.6-jessie

RUN pip3 install -U git+https://github.com/Rapptz/discord.py@rewrite
RUN pip3 install fortnite
RUN pip3 install asyncpg

WORKDIR /opt/bot
COPY main.py ./
COPY cogs ./cogs
CMD [ "python3", "-u", "main.py" ]
