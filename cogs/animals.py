import discord
from discord.ext import commands

import requests
import json

import cogs.data.checks as checks

class Animals:
    def __init__(self, bot):
        self.bot = bot


    @commands.command()
    @commands.check(checks.commands_check)
    async def bird(self, ctx):
        request = requests.get('https://some-random-api.ml/birbimg')
        link = request.json()['link']
        embed = discord.Embed(title="Bird")
        embed.set_image(url=link)
        await ctx.send(embed=embed)


    @commands.command()
    @commands.check(checks.commands_check)
    async def shibe(self, ctx):
        request = requests.get('http://shibe.online/api/shibes')
        link = request.json()[0]
        embed = discord.Embed(title='Shibe')
        embed.set_image(url=link)
        await ctx.send(embed=embed)


    @commands.command()
    @commands.check(checks.commands_check)
    async def dog(self, ctx):
        params = {'limit': 1}
        request = requests.get('http://api.thedogapi.co.uk/v2/dog.php', params=params)
        data = request.json()['data'][0]
        link = data['url']
        embed = discord.Embed(title='Dog')
        embed.set_image(url=link)
        await ctx.send(embed=embed)


    @commands.command()
    @commands.check(checks.commands_check)
    async def cat(self, ctx):
        request = requests.get('https://api.thecatapi.com/v1/images/search?')
        data = request.json()[0]
        link = data['url']
        embed = discord.Embed(title='Cat')
        embed.set_image(url=link)
        await ctx.send(embed=embed)


    @commands.command()
    @commands.check(checks.commands_check)
    async def redpanda(self, ctx):
        request = requests.get('https://some-random-api.ml/redpandaimg')
        link = request.json()['link']
        embed = discord.Embed(title="RedPanda")
        embed.set_image(url=link)
        await ctx.send(embed=embed)


    @commands.command()
    @commands.check(checks.commands_check)
    async def panda(self, ctx):
        request = requests.get('https://some-random-api.ml/pandafact')
        fact = request.json()['fact']

        request = requests.get('https://some-random-api.ml/pandaimg')
        link = request.json()['link']
        embed = discord.Embed(title="Panda", description=fact)
        embed.set_image(url=link)
        await ctx.send(embed=embed)


def setup(bot):
    bot.add_cog(Animals(bot))
def teardown(bot):
    bot.remove_cog('Animals')
