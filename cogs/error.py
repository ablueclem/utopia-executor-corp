import discord
from discord.ext import commands

import sys
import traceback

import cogs.data.checks as checks

class Error:
    def __init__(self, bot):
        self.bot = bot


    async def on_command_error(self, ctx, error):
        if isinstance(error, discord.ext.commands.MissingRequiredArgument):
            embed = discord.Embed(title="Missing Argument",
                                  description="You need to specify a {}.".format(error.param),
                                  color=discord.Color.red())
            await ctx.send(embed=embed, delete_after=8.0)

        elif isinstance(error, discord.ext.commands.CommandNotFound):
            embed = discord.Embed(title="Command Not Found",
                                  description="{} is not a command.".format(ctx.invoked_with),
                                  color=discord.Color.red())
            await ctx.send(embed=embed, delete_after=8.0)

        elif isinstance(error, discord.ext.commands.CommandOnCooldown):
            embed = discord.Embed(title="Command On Cooldown",
                                  description="{} is on a cooldown, please wait {}s.".format(ctx.command.name, error.retry_after),
                                  color=discord.Color.red())
            await ctx.send(embed=embed, delete_after=8.0)

        elif isinstance(error, discord.ext.commands.TooManyArguments):
            embed = discord.Embed(title="Too Many Arguments",
                                  description="{} dont need so many arguments.".format(ctx.command.name),
                                  color=discord.Color.red())
            await ctx.send(embed=embed, delete_after=8.0)

        elif isinstance(error, discord.ext.commands.BadArgument):
            embed = discord.Embed(title="Bad Argument",
                                  description="You gave me a bad argument, check the help to see what i need.",
                                  color=discord.Color.red())
            await ctx.send(embed=embed, delete_after=8.0)

        elif isinstance(error, discord.ext.commands.NoPrivateMessage):
            embed = discord.Embed(title="No Private Message",
                                  description="{} can't be used in DMs".format(ctx.command.name),
                                  color=discord.Color.red())
            await ctx.send(embed=embed, delete_after=8.0)

        elif isinstance(error, discord.Forbidden):
            embed = discord.Embed(title="Fobidden",
                                  description="I don't have permission to do that.",
                                  color=discord.Color.red())
            await ctx.send(embed=embed, delete_after=8.0)

        elif isinstance(error, discord.InvalidArgument):
            embed = discord.Embed(title="Invalid Argument",
                                  description="You gave me an invalid argument.",
                                  color=discord.Color.red())
            await ctx.send(embed=embed, delete_after=8.0)

        elif isinstance(error, discord.HTTPException):
            embed = discord.Embed(title="HTTP Exception",
                                  description="Something went wrong: {}".format(error.text),
                                  color=discord.Color.red())
            await ctx.send(embed=embed, delete_after=8.0)

        elif isinstance(error, discord.NotFound):
            embed = discord.Embed(title="Not Found",
                                  description="That was not found.",
                                  color=discord.Color.red())
            await ctx.send(embed=embed, delete_after=8.0)

        elif isinstance(error, discord.ext.commands.DisabledCommand):
            embed = discord.Embed(title="Disabled Command",
                                  description="{} is disabled".format(ctx.command.name),
                                  color=discord.Color.red())
            await ctx.send(embed=embed, delete_after=8.0)

        elif isinstance(error, discord.ext.commands.CheckFailure):
            embed = discord.Embed(title="Check Failure",
                                  description="You can't use {}.".format(ctx.command.name),
                                  color=discord.Color.red())
            await ctx.send(embed=embed, delete_after=8.0)

        else:
            print('Ignoring exception in command {}:'.format(ctx.command), file=sys.stderr)
            traceback.print_exception(type(error), error, error.__traceback__, file=sys.stderr)
            embed = discord.Embed(title="Welp",
                                  description="Something went wrong, please contact the head dev: `BluePhoenixGame#7543`. Error: ```py\n{}```".format(error),
                                  color=discord.Color.red())
            await ctx.send(embed=embed)


def setup(bot):
    bot.add_cog(Error(bot))
def teardown(bot):
    bot.remove_cog('Error')
