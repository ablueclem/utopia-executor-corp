import discord
from discord.ext import commands

import requests
import json
import random
import pfaw
import asyncio
import re

import cogs.data.checks as checks

fortnite = pfaw.Fortnite(
    fortnite_token='ZWM2ODRiOGM2ODdmNDc5ZmFkZWEzY2IyYWQ4M2Y1YzY6ZTFmMzFjMjExZjI4NDEzMTg2MjYyZDM3YTEzZmM4NGQ=',
    launcher_token='MzQ0NmNkNzI2OTRjNGE0NDg1ZDgxYjc3YWRiYjIxNDE6OTIwOWQ0YTVlMjVhNDU3ZmI5YjA3NDg5ZDMxM2I0MWE=',
    password='Ho#F&NXbkQqkt95K5$Rw', email='fortniteapi@protonmail.com')

class Fun:
    def __init__(self, bot):
        self.bot = bot


    @commands.command(name="8ball", help="Answers a Yes or No question.")
    @commands.check(checks.commands_check)
    async def _ball(self, ctx, *, question: str = "Did i forget to add a question?"):
        answers = {"Yeah": 0x2ecc71, "Yes": 0x2ecc71, "Why not?": 0x2ecc71, "Yup": 0x2ecc71, "What could go wrong?": 0x2ecc71,
                    "Why not?": 0x2ecc71, "Go for it": 0x2ecc71, "True": 0x2ecc71, "Tru": 0x2ecc71, "Correct": 0x2ecc71, "Right": 0x2ecc71,
                    "Pretty sure, yes..": 0x1f8b4c, "Maybe": 0x1f8b4c, "Ehh, think so..": 0x1f8b4c, "Hope so...": 0x1f8b4c, "I don't know..": 0x1f8b4c,
                    "Ask again later!": 0xe67e22, "Don't know": 0xe67e22, "Why are you asking me this?": 0xe67e22, "Ask god...": 0xe67e22, "Im not smart enough to answer that!": 0xe67e22,
                    "Why did you ask?": 0xe67e22, "I don't know how to answer that!": 0xe67e22,
                    "Pay me and i'll answer..": 0xa84300, "Got money?": 0xa84300, "You know, i don't do this for free..": 0xa84300, "Why should i answer that?": 0xa84300,
                    "Hope not": 0xe74c3c, "Ehh, don't think so..": 0xe74c3c, "Not sure": 0xe74c3c, "Not really!": 0xe74c3c, "Very doubtful.": 0xe74c3c,
                    "Nope": 0x992d22, "Don't": 0x992d22, "No": 0x992d22, "Nah!": 0x992d22, "Hell na'": 0x992d22, "Hell no!": 0x992d22}
        answer = random.choice(list(answers.keys()))

        embed = discord.Embed(title=question, description=answer, colour=answers[answer])
        await ctx.send(embed=embed)


    @commands.command(help="Let's you count how high you want.")
    @commands.check(checks.commands_check)
    async def count(self, ctx):
        number = 0
        run = True
        def check(message):
            if ctx.channel.id == message.channel.id and ctx.message.author.id == message.author.id: return True

        await ctx.send("Start counting! You have 5 seconds after each number\nI say 0")
        try:
            while run:
                number += 1
                guess = (await self.bot.wait_for("message", check=check, timeout=5.0)).content

                regex = re.findall(number, guess)
                if len(regex) < 1:
                    await ctx.send("Nope, try again later!")
                    run = False
                    break

        except asyncio.TimeoutError:
            await ctx.send("You took to long to answer, try again!")


    @commands.command(help="Let's you guess a number between 1 and 100.")
    @commands.check(checks.commands_check)
    async def guessnumber(self, ctx):
        number = random.randint(1, 100)
        tries = 0
        run = True
        def check(message):
            if ctx.channel.id == message.channel.id and ctx.message.author.id == message.author.id: return True

        await ctx.send("Guess a number between 1 and 100.")
        try:
            while run:
                guess = int((await self.bot.wait_for("message", check=check)).content)
                tries += 1

                if guess < number:
                    await ctx.send("Guess higher!")
                elif guess > number:
                   await ctx.send("Guess lower!")

                elif guess == number:
                    await ctx.send("Correct! It took {} tries.".format(tries))
                    run = False
                    break

        except asyncio.TimeoutError:
            await ctx.send("You took to long to answer, try again!")


    @commands.command(help="Gives the definition in the given word.")
    @commands.check(checks.commands_check)
    async def urban(self, ctx, *, word: str = "urban"):
        data = (requests.get("http://api.urbandictionary.com/v0/define?term=" + word)).json()["list"][0]
        embed = discord.Embed(title=word[0].upper() + word[1:], description=data["definition"])
        embed.add_field(name="Example", value=data["example"])
        await ctx.send(embed=embed)


    @commands.command(help="Draws a random card.")
    @commands.check(checks.commands_check)
    async def card(self, ctx):
        card = (requests.get("https://deckofcardsapi.com/api/deck/new/draw/?count=1")).json()["cards"][0]['image']
        embed = discord.Embed(colour=discord.Colour.blue())
        embed.set_image(url=card)
        await ctx.send(embed=embed)


    @commands.group(aliases=['ftn', 'fortnut', 'forknife', 'fortnight', 'fortn', 'ftnite'])
    async def fortnite(self, ctx):
        if ctx.invoked_subcommand is None:
            await ctx.send("That is not a mode")

    @fortnite.command(help="Shows if the fortnite servers are up or not.")
    @commands.check(checks.commands_check)
    async def servers(self, ctx):
        key = {True: "They are up and running.", False: "Seems like some downtime."}
        embed = discord.Embed(title="Fortnte servers", description=key[fortnite.server_status()], colour=discord.Colour.purple())
        await ctx.send(embed=embed)

    @fortnite.command(help="Gives you stats from the given player. For all solo matches.")
    @commands.check(checks.commands_check)
    async def solo(self, ctx, name: str = "BluePhoenixGamer"):
        user = fortnite.battle_royale_stats(username=name, platform=pfaw.Platform.pc)
        embed = discord.Embed(title="Fortnite stats", description="{}'s Solo Fortnite Battle Royale stats.".format(name), colour=discord.Colour.purple())
        embed.add_field(name="Matches", value=user.solo.matches)
        embed.add_field(name="Wins", value=user.solo.wins)
        embed.add_field(name="Kills", value=user.solo.kills)
        embed.add_field(name="Time", value=user.solo.time)
        embed.add_field(name="Score", value=user.solo.score)
        embed.add_field(name="Top 25", value=user.solo.top25)
        await ctx.send(embed=embed)

    @fortnite.command(help="Gives you stats from the given player. For all duo matches.")
    @commands.check(checks.commands_check)
    async def duo(self, ctx, name: str = "BluePhoenixGamer"):
        user = fortnite.battle_royale_stats(username=name, platform=pfaw.Platform.pc)
        embed = discord.Embed(title="Fortnite stats", description="{}'s Duo Fortnite Battle Royale stats.".format(name), colour=discord.Colour.purple())
        embed.add_field(name="Matches", value=user.duo.matches)
        embed.add_field(name="Wins", value=user.duo.wins)
        embed.add_field(name="Kills", value=user.duo.kills)
        embed.add_field(name="Time", value=user.duo.time)
        embed.add_field(name="Score", value=user.duo.score)
        embed.add_field(name="Top 25", value=user.duo.top25)
        await ctx.send(embed=embed)

    @fortnite.command(help="Gives you stats from the given player. For all squad matches.")
    @commands.check(checks.commands_check)
    async def squad(self, ctx, name: str = "BluePhoenixGamer"):
        user = fortnite.battle_royale_stats(username=name, platform=pfaw.Platform.pc)
        embed = discord.Embed(title="Fortnite stats", description="{}'s Squad Fortnite Battle Royale stats.".format(name), colour=discord.Colour.purple())
        embed.add_field(name="Matches", value=user.squad.matches)
        embed.add_field(name="Wins", value=user.squad.wins)
        embed.add_field(name="Kills", value=user.squad.kills)
        embed.add_field(name="Time", value=user.squad.time)
        embed.add_field(name="Score", value=user.squad.score)
        embed.add_field(name="Top 25", value=user.squad.top25)
        await ctx.send(embed=embed)

    @fortnite.command(help="Gives you stats from the given player. For all matches.")
    @commands.check(checks.commands_check)
    async def all(self, ctx, name: str = "BluePhoenixGamer"):
        user = fortnite.battle_royale_stats(username=name, platform=pfaw.Platform.pc)
        embed = discord.Embed(title="Fortnite stats", description="{}'s All Fortnite Battle Royale stats.".format(name), colour=discord.Colour.purple())
        embed.add_field(name="Matches", value=user.all.matches)
        embed.add_field(name="Wins", value=user.all.wins)
        embed.add_field(name="Kills", value=user.all.kills)
        embed.add_field(name="Time", value=user.all.time)
        embed.add_field(name="Score", value=user.all.score)
        embed.add_field(name="Top 25", value=user.all.top25)
        await ctx.send(embed=embed)

    @fortnite.command(help="Sends the current news in fortnite.")
    @commands.check(checks.commands_check)
    async def news(self, ctx):
        for news in (fortnite.news()).br:
            embed = discord.Embed(title=news.title, description=news.body, colour=discord.Colour.purple())
            embed.set_image(url=news.image)
            await ctx.send(embed=embed)


def setup(bot):
    bot.add_cog(Fun(bot))
def teardown(bot):
    bot.remove_cog('Fun')
