import discord
from discord.ext import commands

import datetime
import asyncio

import cogs.data.database as database
import cogs.data.checks as checks


async def queue():
    while True:
        week = int(datetime.datetime.utcnow().strftime("%V"))
        while int(datetime.datetime.utcnow().weekday()) is 4:
            if int(datetime.datetime.utcnow().hour) is 12:
                game = await database.get_game("VIEW", week)
                if game is None:
                    break

                embed = discord.Embed(title=game[1], description=game[2], colour=int(game[3], 16))
                embed.set_thumbnail(url=game[6])
                embed.add_field(name="Players", value=game[5])
                embed.add_field(name="Systems", value=game[4])
                embed.add_field(name="Publisher", value=game[8])
                embed.add_field(name="Download", value="[{}]({})".format(game[1], game[7]))
                await self.bot.get_channel(config.gamenight_channel).send(embed=embed)
            await asyncio.sleep(3600)

        await asyncio.sleep(86400)


class Gamenight:
    def __init__(self, bot):
        self.bot = bot


    @commands.command(help="Sends the current planned gamenight.")
    @commands.check(checks.utopia_check)
    @commands.check(checks.commands_check)
    async def gamenight(self, ctx, view: bool = True):
        if view is True:
            week = datetime.datetime.utcnow().strftime("%V")
            game = await database.get_game("VIEW", int(week))
            if game is None:
                return await ctx.send("There is no game for this week yet.")

            embed = discord.Embed(title=game[1], description=game[2], colour=int(game[3], 16))
            embed.set_thumbnail(url=game[6])
            embed.add_field(name="Players", value=game[5])
            embed.add_field(name="Systems", value=game[4])
            embed.add_field(name="Publisher", value=game[8])
            embed.add_field(name="Download", value="[{}]({})".format(game[1], game[7]))
            await ctx.send(embed=embed)

        elif view is False and await database.get_staff(ctx.message.guild.id):
            def check(message):
                return message.author.id == ctx.message.author.id and message.channel.id == ctx.message.channel.id

            await ctx.send("Image link:")
            image_url = (await self.bot.wait_for("message", check=check)).content
            await ctx.send("Download link:")
            download_url = (await self.bot.wait_for("message", check=check)).content
            await ctx.send("Publisher:")
            publisher = (await self.bot.wait_for("message", check=check)).content

            await ctx.send("Name:")
            name = (await self.bot.wait_for("message", check=check)).content
            await ctx.send("Description:")
            description = (await self.bot.wait_for("message", check=check)).content
            await ctx.send("Colour:")
            colour = (await self.bot.wait_for("message", check=check)).content

            await ctx.send("Systems:")
            systems = (await self.bot.wait_for("message", check=check)).content
            await ctx.send("Players: ")
            players = (await self.bot.wait_for("message", check=check)).content

            await ctx.send("Week:")
            week = (await self.bot.wait_for("message", check=check)).content

            await database.create_game(week, name, description, colour, systems, players, image_url, download_url, publisher)

            embed = discord.Embed(title=name, description=description, colour=int(colour, 16))
            embed.set_thumbnail(url=image_url)
            embed.add_field(name="Players", value=players)
            embed.add_field(name="Systems", value=systems)
            embed.add_field(name="Publisher", value=publisher)
            embed.add_field(name="Download", value="[{}]({})".format(name, download_url))
            await ctx.send(embed=embed)
        else:
            return await ctx.send("Sorry, what?")


def setup(bot):
    asyncio.ensure_future(queue())
    bot.add_cog(Gamenight(bot))
def teardown(bot):
    bot.remove_cog('Gamenight')
