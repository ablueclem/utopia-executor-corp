import discord
from discord.ext import commands

import re

import cogs.data.checks as checks


class Help:
    def __init__(self, bot):
        self.bot = bot


    def command_search(self, search, command):
        regex = re.findall(search.lower(), str(command.name).lower())
        if len(regex) > 0:
            return True


    @commands.command(help="Shows all commands- how you use them and what they do.")
    @commands.check(checks.commands_check)
    async def help(self, ctx, search: str = '.'):
        help_msg = ""

        for cog in self.bot.cogs:
            first_command = True
            if len(self.bot.get_cog_commands(cog)) <= 0: continue
            if len(help_msg) > 1500:
                await ctx.send(help_msg)
                help_msg = ""

            for command in self.bot.get_cog_commands(cog):
                if self.command_search(search, command) is True:

                    if first_command is True:
                        help_msg += "\n__**{}**__\n".format(cog)
                        first_command = False

                    help_msg += "   **{}** - {}\n".format(command.signature, command.help)
                    if len(command.aliases) > 0:
                        help_msg += "       *{}*".format(" ".join(command.aliases))

        if len(help_msg) <= 0:
            help_msg += "We couldn't find any commands matching your search."
        await ctx.send(help_msg)


def setup(bot):
    bot.add_cog(Help(bot))
def teardown(bot):
    bot.remove_cog('Help')
