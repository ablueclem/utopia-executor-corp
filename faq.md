#Thanks for helping!#

#Contribute#

##How?##
Fork the repo and create a branch, then create a pull request into the experimental branch. NOT master!!!
Currently 1.1 is the experimental

##Why?##
Who doesnt want to be able to say, i made that. Or see someone use what you made.

#Issue#

##How?##
Just click the 'Issues' tab and there should be a place where you can see 'Create Issue' Then fill in the form and we'll try to fix it.

##Why?##
We would like the bot to be as stable as possible. And we would also like the bot to have all the features that **you** want the bot to have.